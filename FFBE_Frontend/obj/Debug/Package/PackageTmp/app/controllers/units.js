app.controller('unit', function ($scope, $http) {
    

    $scope.getUnits = function () {

        


        swal({
            title: 'Summoneando',
            text: 'Espere por favor..',
            imageUrl: '/img/chocobo.gif'
        })

        var url = 'http://jffbedb.herokuapp.com/api/units/list';
        $http({
            method: 'GET',
            url: url

        }).then(function (response) {
            
            var list = response.data;
            $scope.unitList = [];
            for (item in list) {            

                $scope.setRank(list[item]);
                $scope.unitList.push(list[item]);
            }

            //$scope.unitList 

            console.log($scope.unitList);
            swal({
                title: 'Summoneando',
                text: 'Espere por favor..',
                imageUrl: 'https://thumbs.gfycat.com/BitterEminentIberianmole-max-1mb.gif',
                timer:200
            })

        }, function (c) {
        });
    }

    $scope.setRank= function setRank(obj)
    {

      
        var num = Math.floor((Math.random() * 6) + 1);
        switch (num)
        {
            case 1: obj.rank = "C"; break;
            case 2: obj.rank = "B"; break;
            case 3: obj.rank = "A"; break;
            case 4: obj.rank = "AA"; break;
            case 5: obj.rank = "S"; break;
            case 6: obj.rank = "SS"; break;
        }
        
    }

    $scope.getUnit = function () {
        
        var id = getParameterByName('unitID');

        if (id == null)
            window.location = "/index.html";

        var url = 'http://jffbedb.herokuapp.com/api/units/' + id;
        $http({
            method: 'GET',
            url: url

        }).then(function (response) {
            $scope.unitDetail = response.data;
            console.log($scope.unitDetail);
            var currententries = [];

            for (item in $scope.unitDetail.entries) {
                //$scope.unitDetail.stats0 = JSON.parse($scope.unitDetail.entries[item].stats);
                //console.log($scope.unitDetail.entries[item]);
                //$scope.unitDetail.entries[item].statsJson = JSON.parse($scope.unitDetail.entries[item].stats);
                //$scope.unitDetail.entries[item].stringsJson = JSON.parse($scope.unitDetail.entries[item].strings);
                //$scope.unitDetail.element_resistJson = JSON.parse($scope.unitDetail.entries[item].element_resist);
                //$scope.unitDetail.status_resistJson = JSON.parse($scope.unitDetail.entries[item].status_resist);
                currententries.push(item);
                $scope.getLimitBurst($scope.unitDetail.entries[item]);
            }
             $scope.unitDetail.currentID = currententries[0];

            $scope.getJob($scope.unitDetail);


            //$scope.skillsJSON = Object.keys($scope.skills).map(function (key) {
            //        return $scope.skills[key];  });

            $scope.unitDetail.equipJSONFull = {};

            var equips = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 30, 31, 40, 41, 50, 51, 52, 53];

            for (var i = 0; i < equips.length; i++) {
                $scope.unitDetail.equipJSONFull[equips[i]] = 0;
            }

            $scope.unitDetail.equipJSON = JSON.parse($scope.unitDetail.equip);


            for (var i = 0; i < $scope.unitDetail.equipJSON.length; i++) {
                for (item in $scope.unitDetail.equipJSONFull)
                {
                    if ($scope.unitDetail.equipJSON[i] ==[item])
                        $scope.unitDetail.equipJSONFull[item] = 1;
                }
            }

            console.log($scope.unitDetail);



           


            //$scope.unitDetail.skillsJSON = JSON.parse($scope.unitDetail.skills);

            $scope.unitDetail.skillsJSON = [];
            for (var item in $scope.unitDetail.skills) {
                $scope.getSkills(item, $scope.unitDetail.skills[item].level);
            }


    
            $scope.getTrustMaster($scope.unitDetail);
       
        
           
            setInterval(myMethod, 2500);
            var current = 0;

            function myMethod() {
                if (current >= currententries.length)
                    current = 0;
                $scope.unitDetail.currentID = currententries[current];
                current++;
                $scope.$apply();
            }

            console.log($scope.unitDetail);

        }, function (c) {
        });
    }


    $scope.getLimitBurst = function (obj) {
        
        var url = 'http://jffbedb.herokuapp.com/api/limitbursts/' + obj.limitburst_id;
        $http({
            method: 'GET',
            url: url

        }).then(function (response) {
            obj.limit = response.data;    
            obj.limit.attack_count = JSON.parse(obj.limit.attack_count);
            //obj.limit.max_level = JSON.parse(obj.limit.max_level); 
            //obj.limit.min_level = JSON.parse(obj.limit.min_level); 

        }, function (c) {
        });
    }



    $scope.getSkills = function (obj,level) {
        
        var url = 'http://jffbedb.herokuapp.com/api/skills/' + obj;
        $http({
            method: 'GET',
            url: url

        }).then(function (response) {
            var obj = response.data;
            console.log(obj);
            obj.level = level;                   
            //obj.effects = JSON.parse(obj.effects); 
            //obj.active = JSON.parse(obj.active);
            obj.attack_count = JSON.parse(obj.attack_count); 
            $scope.unitDetail.skillsJSON.push(obj);
            
        }, function (c) {
        });
    }



    $scope.getJob = function (obj) {

        var url = 'http://jffbedb.herokuapp.com/api/jobs/' + obj.job_id;
        $http({
            method: 'GET',
            url: url

        }).then(function (response) {
            obj.jobData = response.data;
        }, function (c) {
        });
    }


    $scope.getTrustMaster = function (obj) {

        var route = "none";
        switch (obj.TMR_type)
        {
            case "EQUIP": route = "equipment"; break;
            case "MATERIA": route = "materias"; break;           
        }

        var url = 'http://jffbedb.herokuapp.com/api/' + route+'/' + obj.TMR_id;
        $http({
            method: 'GET',
            url: url

        }).then(function (response) {
            obj.TMR = response.data;
            //obj.TMR.effects = JSON.parse(obj.TMR.effects);
            obj.TMR.skills = JSON.parse(obj.TMR.skills);
            try{
                obj.TMR.stats = JSON.parse(obj.TMR.stats);
            }
            catch(exception)
            {
            }


            try {
                if (obj.TMR.skills.length > 0)
                    $scope.getSkillID(obj.TMR);
            }
            catch (exception) {
            }
            $scope.getEquipmentType(obj.TMR);

        }, function (c) {
        });
    }

    $scope.getEquipmentType= function (TMR) {

        console.log("TMR");
        console.log(TMR);

        var url = 'http://jffbedb.herokuapp.com/api/equipmenttypes/' + TMR.type_id;
            $http({
                method: 'GET',
                url: url

            }).then(function (response) {

                TMR.type = response.data;

            }, function (c) {
            });

        
    }



    $scope.getSkillID = function (TMR) {

        TMR.skillsDetail = [];

        for (var i = 0; i < TMR.skills.length; i++) {
            

          
            var url = 'http://jffbedb.herokuapp.com/api/skills/' + TMR.skills[i];
            $http({
                method: 'GET',
                url: url

            }).then(function (response) {

                var obj = response.data
                //obj.effectsJSON = JSON.parse(obj.effects); 
                TMR.skillsDetail.push( obj);
               

            }, function (c) {
                });

        }
    }


});







function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
